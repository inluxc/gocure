module gitlab.com/rodrigoodhin/gocure

go 1.17

require (
	github.com/arsmn/fiber-swagger/v2 v2.24.0
	github.com/gofiber/fiber/v2 v2.27.0
	github.com/swaggo/swag v1.8.0
	github.com/tdewolff/minify/v2 v2.10.0
	github.com/u2takey/ffmpeg-go v0.4.1
)

require (
	github.com/BurntSushi/toml v1.0.0 // indirect
	github.com/KyleBanks/depth v1.2.1 // indirect
	github.com/PuerkitoBio/purell v1.1.1 // indirect
	github.com/PuerkitoBio/urlesc v0.0.0-20170810143723-de5bf2ad4578 // indirect
	github.com/andybalholm/brotli v1.0.4 // indirect
	github.com/aws/aws-sdk-go v1.38.20 // indirect
	github.com/go-openapi/jsonpointer v0.19.5 // indirect
	github.com/go-openapi/jsonreference v0.19.6 // indirect
	github.com/go-openapi/spec v0.20.4 // indirect
	github.com/go-openapi/swag v0.19.15 // indirect
	github.com/google/go-cmp v0.5.7 // indirect
	github.com/haya14busa/goplay v1.0.0 // indirect
	github.com/jmespath/go-jmespath v0.4.0 // indirect
	github.com/josharian/intern v1.0.0 // indirect
	github.com/klauspost/compress v1.14.1 // indirect
	github.com/mailru/easyjson v0.7.6 // indirect
	github.com/sergi/go-diff v1.1.0 // indirect
	github.com/skratchdot/open-golang v0.0.0-20200116055534-eef842397966 // indirect
	github.com/swaggo/files v0.0.0-20210815190702-a29dd2bc99b2 // indirect
	github.com/tdewolff/parse/v2 v2.5.27 // indirect
	github.com/u2takey/go-utils v0.3.1 // indirect
	github.com/valyala/bytebufferpool v1.0.0 // indirect
	github.com/valyala/fasthttp v1.33.0 // indirect
	github.com/valyala/tcplisten v1.0.0 // indirect
	golang.org/x/exp/typeparams v0.0.0-20220218215828-6cf2b201936e // indirect
	golang.org/x/mod v0.6.0-dev.0.20220419223038-86c51ed26bb4 // indirect
	golang.org/x/net v0.0.0-20220111093109-d55c255bac03 // indirect
	golang.org/x/sync v0.0.0-20210220032951-036812b2e83c // indirect
	golang.org/x/sys v0.0.0-20220209214540-3681064d5158 // indirect
	golang.org/x/text v0.3.7 // indirect
	golang.org/x/tools v0.1.12-0.20220713141851-7464a5a40219 // indirect
	golang.org/x/tools/gopls v0.9.1 // indirect
	golang.org/x/vuln v0.0.0-20220613164644-4eb5ba49563c // indirect
	gopkg.in/yaml.v2 v2.4.0 // indirect
	gopkg.in/yaml.v3 v3.0.0-20210107192922-496545a6307b // indirect
	honnef.co/go/tools v0.3.2 // indirect
	mvdan.cc/gofumpt v0.3.0 // indirect
	mvdan.cc/xurls/v2 v2.4.0 // indirect
)
